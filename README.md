Dockerize Vuexy application
============

Description
-----------
This project aims to dockerize a Vuexy application, which is a template built on top of Vue.js 2. The Dockerfile and docker-compose files are provided to create a Docker image and define the deployment using Docker Compose. Additionally, a useful script has been included to automate the deployment process.

Prerequisites
-------------
Before proceeding with the deployment, ensure that the following dependencies are installed on your system:

- Docker
- Docker Compose

Installation
------------
1. Clone the repository to your local machine:
   git clone https://gitlab.com/angosil/dockerize-vuexy-application

2. Navigate to the project directory:
   cd dockerize-vuexy-application

3. Build the Docker image using the provided Dockerfile:
   docker build -t vuexy-app .

Usage
-----
To start the application and manage the deployment, you can use the provided script `scripts/restart.dev.sh` instead of the `docker-compose up` command. This script automates the process and provides additional functionality for development purposes.

To use the script, follow these steps:

1. Open a terminal and navigate to the project directory:
   cd dockerize-vuexy-application

2. Run the script to start the application and Docker containers:
   ./scripts/restart.dev.sh

   The script performs the following actions:
   - Optional: Removes existing Docker images (use `-r` or `--restart` flag to enable).
   - Builds and starts the Docker containers using Docker Compose.
   - Waits for the app service to be available.
   - Displays the status of the services.
   - Shows the logs of the app service.

   Once the script completes, the application will be up and running. Access it by visiting http://localhost:8092 in your web browser.

Configuration
-------------
The Docker Compose file (`docker-compose.yml`) includes default configurations for the application. However, you can modify these configurations according to your needs. Make sure to update the necessary environment variables and container settings before deploying the application.

Contributing
------------
If you'd like to contribute to this project, please follow the guidelines outlined in the CONTRIBUTING.md file.

License
-------
This project is licensed under the MIT License.

Acknowledgments
---------------
- Vuexy - The original Vuexy template used in this project.

Notes: Using Docker on Raspberry Pi
----------------------------------
If you are using Docker on a Raspberry Pi, you may encounter issues when using the `docker-compose` command. One solution is to create a little script.

1. Create the file `/bin/docker-compose`:
sudo nano /bin/docker-compose


2. Add the following content to the file:
#!/bin/bash
docker compose “$@”


Alternatively, to respect the previous container naming convention (with underscore delimiter `_` over `-`):
#!/bin/bash
docker compose --compatibility “$@”


3. Make the script executable:
sudo chmod +x /bin/docker-compose


You can now use `docker-compose` as usual. To check the installation, run `docker-compose version`.

Reference: [Stack Overflow - How to alias docker-compose to docker compose](https://stackoverflow.com/questions/72099653/how-to-alias-docker-compose-to-docker-compose/72187587#72187587)

