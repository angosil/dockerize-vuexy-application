#!/bin/bash
# Version : 1.0.0
# Date: 2023-05-01
# Source: ana-toolbox
# Objective: Run a Docker container with a Vuexy full template.
# Description: This script is focused on development to show and control important information about the process.
# ChangeLog:
# - Add new optional first stage with a new description.
# - Two stages with lists of services and the logs.

remove_docker_images() {
  echo "Removing docker images"
  docker-compose -f docker-compose.yaml down --rmi all -v
}

if [[ $1 = "-r" || $1 = "--restart" ]]; then
  echo
  echo "1) Optional restarting project"
  remove_docker_images
fi

echo
echo "2) Starting docker containers"
docker-compose -f docker-compose.yaml up --build -d || exit

echo
echo "3) Waiting for the app service"
while ! curl http://127.0.0.1:8092 -m1 -o/dev/null -s; do
  echo "Waiting ..."
  sleep 1
done

echo
echo "4) Services status"
docker-compose -f docker-compose.yaml ps

echo
echo "5) Showing logs"
docker-compose logs app

echo
echo "App is up"
