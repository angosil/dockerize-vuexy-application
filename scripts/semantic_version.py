import subprocess
from datetime import datetime


def generate():
    # Get the current version from the latest Git tag
    try:
        tag = subprocess.check_output(['git', 'describe', '--abbrev=0', '--tags']).strip().decode()
        major, minor, patch = tag.split('.')
        tag = f"{major}.{minor}.{int(patch) + 1}"
    except subprocess.CalledProcessError:
        # No tags found, start with initial version
        now = datetime.now()
        year = now.strftime("%y")
        month = now.strftime("%m")
        tag = f"{year}.{get_month(month)}.0"

    return tag


def get_month(month):
    if month <= '04':
        return '04'
    elif month <= '10':
        return '10'
    else:
        return '04'


if __name__ == '__main__':
    print(generate())
